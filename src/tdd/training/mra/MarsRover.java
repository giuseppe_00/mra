package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {

	
	private int PlanetX=0;
	private int PlanetY=0;
	private String rover=null;
	private List<String> PlanetObstacles=new ArrayList<>();
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		// To be implemented
		PlanetX=planetX;
		PlanetY=planetY;
		PlanetObstacles= planetObstacles;
        rover="(0,0,N)";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		// To be implemented
		String obstacle="("+x+","+y+")";
		if(PlanetObstacles.contains(obstacle))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		// To be implemented
		String com;
		for(int i=0;i<commandString.length();i++)
		{
			com=""+commandString.charAt(i);
			switch(com)
			{
			     case " ": return rover;
			     case "r":rightStateRover();break;
			    	     
			     case "l":leftStateRover();break;
	   	                  
			     case "f":forwardStateRover();break;
			    	      
			     case "b":backwardStateRover();break;
	   	                  
			}
		}
		
		return rover;
	}

	public void forwardStateRover() throws MarsRoverException
	{
		char dir=rover.charAt(5);
		int num=0;
		if(dir=='N')
		{
			num=rover.charAt(3)-'0'+1;
   	        rover=rover.substring(0,3)+num+rover.substring(4,6)+")";
		}
		if(dir=='S')
		{
			num=rover.charAt(3)-'0'-1;
            rover=rover.substring(0,3)+num+rover.substring(5)+")";
		}
		if(dir=='E')
		{
			num=rover.charAt(1)-'0'+1;
            rover="("+num+rover.substring(2,6)+")";
		}
		if(dir=='W')
		{
			num=rover.charAt(1)-'0'-1;
               rover="("+num+rover.substring(2,6)+")";
		}	
	}
	public void backwardStateRover() throws MarsRoverException
	{
		char dir=rover.charAt(5);
		int num=0;
		if(dir=='N')
		{
			num=rover.charAt(3)-'0'-1;
   	        rover=rover.substring(0,3)+num+rover.substring(4,6)+")";
		}
		if(dir=='S')
		{
			num=rover.charAt(3)-'0'+1;
            rover=rover.substring(0,3)+num+rover.substring(5)+")";
		}
		if(dir=='E')
		{
			num=rover.charAt(1)-'0'-1;
            rover="("+num+rover.substring(0,6)+")";
		}
		if(dir=='W')
		{
			num=rover.charAt(1)-'0'+1;
               rover="("+num+rover.substring(0,6)+")";
		}	
	}
	
	public void rightStateRover() throws MarsRoverException
	{
		char dir=rover.charAt(5);
		switch(dir)
		{
		   case 'N':rover=rover.substring(0,5) +"E)";
		            break;
		   case 'S':rover=rover.substring(0,5) +"W)";
                    break;
		   case 'W':rover=rover.substring(0,5) +"N)";
                    break;
		   case 'E':rover=rover.substring(0,5) +"S)";
                    break;
		}
	}
	
	public void leftStateRover() throws MarsRoverException
	{
		char dir=rover.charAt(5);
		switch(dir)
		{
		   case 'N':rover=rover.substring(0,5) +"W)";
		            break;
		   case 'S':rover=rover.substring(0,5) +"E)";
                    break;
		   case 'W':rover=rover.substring(0,5) +"S)";
                    break;
		   case 'E':rover=rover.substring(0,5) +"N)";
                    break;
		}
	}
	
	public int wrapX(int num, int action)
	{
		if(num==0 && action==-1)
		{
			return PlanetX;
		}
		else
		{
			if(num==9 && action==1)
			{
				return 0;
			}
		}
		return 0;
	}
	public int wrapY(int num, int action)
	{
		if(num==0 && action==-1)
		{
			return PlanetX;
		}
		else
		{
			if(num==9 && action==1)
			{
				return 0;
			}
		}
		return 0;
	}
}
