package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetContainsObstacleAt() throws MarsRoverException {
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(4,7)");
		MarsRover mers=new MarsRover(10, 10,obstacles);
		
		
		assertTrue(mers.planetContainsObstacleAt(4, 7));
	}
	@Test
	public void testLandingRover() throws MarsRoverException {
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(4,7)");
		MarsRover mers=new MarsRover(10, 10,obstacles);
		
		
		assertEquals("(0,0,N)",mers.executeCommand(" "));
	}
	
	@Test
	public void testRightRover() throws MarsRoverException {
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(4,7)");
		MarsRover mers=new MarsRover(10, 10,obstacles);
		
		
		assertEquals("(0,0,E)",mers.executeCommand("r"));
	}
	
	@Test
	public void testForwardRover() throws MarsRoverException {
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(4,7)");
		MarsRover mers=new MarsRover(10, 10,obstacles);
		
		
		assertEquals("(0,1,N)",mers.executeCommand("f"));
	}
	
	@Test
	public void testBackwardRover() throws MarsRoverException {
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(4,7)");
		MarsRover mers=new MarsRover(10, 10,obstacles);
		mers.executeCommand("f");
		mers.executeCommand("f");
		mers.executeCommand("f");
		
		assertEquals("(0,2,N)",mers.executeCommand("b"));
	}
	@Test
	public void testMultipleCommandsRover() throws MarsRoverException {
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(4,7)");
		MarsRover mers=new MarsRover(10, 10,obstacles);
		
		
		assertEquals("(2,2,E)",mers.executeCommand("ffrff"));
	}
	@Test
	public void testWrappingCommandsRover() throws MarsRoverException {
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(4,7)");
		MarsRover mers=new MarsRover(10, 10,obstacles);
		
		
		assertEquals("(0,9,N)",mers.executeCommand("b"));
	}

}
